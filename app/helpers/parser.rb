## Will parse csv file content to the seed DB
module Parser
  def self.parser(file, object)
    balance = []
    CSV.foreach(file, headers: true, header_converters: :symbol) do |csv_obj|
      balance << object.new(csv_obj)
    end
    balance
  end
end