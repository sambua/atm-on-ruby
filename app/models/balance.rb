$db = SQLite3::Database.open 'atm.db'

## Balance model
class Balance
  attr_reader :code, :balance_left

  def initialize(*args)
    args = args[0]
    @code = args[:code]
    @balance_left = args[:balance_left]
  end

  def self.sorted_data
    $db.execute('SELECT * FROM balance WHERE balance_left > 0 ORDER BY code DESC')
  end

  def self.current_code_balance
    $db.execute("SELECT SUM(balance_left) FROM balance WHERE code = #{code}")
  end

  def self.where(attribute)
    query = "SELECT * FROM balance where #{attribute}"
    id = $db.execute(query).flatten
  end

  def self.code_exists(code)
    $db.execute("SELECT 1 FROM balance WHERE code = #{code}").length.positive?
  end

  def self.calculate_withdraw(amount)
    left = amount
    # output = [errors: [], result: []]
    output = Hash.new { |hash, key| hash[key] = Hash.new { |k, v| k[v] = [] } }
    # output = Hash.new { |hash, key| hash[key] = [] }
    banknotes = _get_all_available_banknotes
    if banknotes.length.positive?
      min = banknotes[banknotes.length - 1]
      banknotes.each do |code, balance_left|
        times = left / code
        # If amount is
        if times.positive?
          available = balance_left >= times ? times : balance_left
          left -= available * code
          # output[:result].push(Array.new(code, available))
          output[:result][code] = available
        end
        if code == min[0].to_i && left != 0
          output[:errors][:message] << "Sorry currently we can withdraw only #{amount - left} as some banknotes are missing"
          return output
        end

        next unless left.zero?

        if output[:result].length.positive?
          begin
            output[:result].each do |key, value|
              minus_from_balance key, value
            end
            output
          rescue SQLite3::Exception => e
            {
              message: "Error occurred during DB operation on withdraw #{e}"
            }
          end
        end
      end

    else
      output[:errors][:message] << 'Sorry ATM is empty for now, please try later'
    end
    output
  end

  def insert
    inset = <<-SQL
      INSERT INTO balance (id, code, balance_left, created_at, updated_at)
      values (NULL, ?, ?, DATETIME('now'), DATETIME('now'))
    SQL
    $db.execute(
      inset,
      code,
      balance_left
    )
  end

  def top_up_balance
    update = <<-SQL
      UPDATE balance
      SET balance_left = balance_left + ?,  updated_at = DATETIME('now')
      WHERE code = ?
    SQL
    $db.execute(
      update,
      balance_left,
      code
    )
  end
  
  def self.minus_from_balance(code, value)
    update = <<-SQL
      UPDATE balance
      SET balance_left = balance_left - ?,  updated_at = DATETIME('now')
      WHERE code = ?
    SQL
    $db.execute(
      update,
      value,
      code
    )
  end

  private

  def self._get_all_available_banknotes
    query = 'SELECT code, balance_left FROM balance WHERE balance_left > 0 ORDER BY code DESC'
    $db.execute(query)
  end

end