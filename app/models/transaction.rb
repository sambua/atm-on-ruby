$db = SQLite3::Database.open 'atm.db'

## Transaction model
class Transaction
  attr_reader :amount, :data, :created_at

  def initialize(*args)
    args = args[0]
    @amount = args[:amount]
    @data = args[:data]
    @created_at = args[:created_at]
  end

  def self.sorted_data
    sorted = $db.execute('SELECT * FROM transactions')
  end

  def self.where(attribute, value)
    query = "SELECT * FROM transactions WHERE #{attribute}"
    id = $db.execute(query, value).flatten
  end

  def save
    inset = <<-SQL
      INSERT INTO balance
      values (NULL, ?,?,?, DATETIME('now'))
    SQL
    $db.execute(
      inset,
      amount,
      data,
      created_at
    )
  end
end