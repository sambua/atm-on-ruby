require_relative '../../config/application'
require 'grape_logging'

## ATM Module
module ATM

  ## AtmAPI class
  class AtmAPI < Grape::API
    logger.formatter = GrapeLogging::Formatters::Default.new
    use GrapeLogging::Middleware::RequestLogger, { logger: logger }
    version 'v1', using: :path # Using this versioning strategy, clients should pass the desired version in the URL
    format :json
    rescue_from :all
    # prefix :api

    error_formatter :json, lambda { |message, backtrace, options, env, original_exception|
      {
        status: 'failed',
        message: message,
        error_code: '400'
        # error_code: env['api.endpoint'].status <-- In wrong request get unexpected error
      }
    }

    helpers do
      def existing_banknote?(code)
        [1, 2, 5, 10, 25, 50].include? code
      end
    end

    resource '/atm' do
      get do
        # We can list all balance
        {
          message: 'We have only POST and PUT methods for this resource, POST will add money to ATM, PUT will withdraw'
        }
      end

      desc "Let's top-up the balance"
      # params do
      #   requires :data, type: Hash, desc: 'Should include correct money types and value'
      # end
      params do
        requires :balance, type: Hash, desc: 'We have to get, key value data'
      end
      options :balance, type: Array do
        require :key
        require :value
      end
      post do
        total_amount = 0
        begin
          params[:balance].each do |code, qty|
            code = code.to_i if code.instance_of? String

            raise "Non allowed banknote was provided: #{code}" unless existing_banknote? code
            raise "You can only withdraw with value: #{qty}" unless qty.positive?

            amount = code * qty
            bal = Balance.new(code: code, balance_left: qty)
            if Balance.code_exists code
              bal.top_up_balance
            else
              bal.insert
            end
            total_amount += amount
          end
        rescue SQLite3::Exception => e
          {
            message: "Error occurred during DB operation #{e}"
          }
        end
        {
          message: "All success we have top-up ATM Balance for #{total_amount}"
        }
        # JSON(params)
      end

      desc 'Now withdraw money from here'
      params do
        requires :withdraw, type: Integer, desc: 'Only Natural number is allowed'
      end
      put do
        amount = params[:withdraw]
        raise "Only Positive Amount allowed, you provided amount is: #{amount}" unless amount.positive?

        Balance.calculate_withdraw amount
      end
    end
  end
end
