TASK Requirements:
1) An endpoint for adding money to the ATM by specifying an amount of each banknote. Available banknotes: 1, 2, 5, 10, 25, 50.
   Example of payload: {1: 10, 10: 10, 50: 10}

2) An endpoint for withdrawal that accepts an integer value and returns banknotes.
   Example:
   Entered value - 85, a possible response - {50: 1, 10: 3, 1: 5}

3) Available banknotes amount should reduce with every withdrawal.

4) The task should be implemented as a JSON API. The preferred API framework is Grape.

### To run application:
- `rake db:create` Will create DB
- `rake db:seed` Will seed balance DB, only part missing 5 and 10 for correct testing purposes
- `bundle exec rackup` Will start the server, probably in the port 9292, will be better to read exact port from the terminal

After steps above now we have to have server running on `http://localhost:9292/v1/atm`

- `curl -X POST '127.0.0.1:9292/v1/atm' -H "Content-Type: application/json" -H  "accept: application/json" -d '{"balance":{"1": 50, "2": 10, "5": 3, "10": 15}}'`
- `curl -X PUT '127.0.0.1:9292/v1/atm' -H "Content-Type: application/json" -H  "accept: application/json" -d '{"withdraw": 85}'`

### Delete DB and reset App:
- `rake db:drop`

### Fix Already in use
- `lsof -wni tcp:9292` find running PID number
- `kill -9 PID_NUMBER` kill PID number and run server again