$db = SQLite3::Database.new 'atm.db'
## ATM DB Module
module AtmDB
  def self.create_balance_table(database)
    database.execute(
      <<-SQL
      CREATE TABLE balance (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        code INTEGER UNIQUE NOT NULL,
        balance_left INTEGER NOT NULL,
        created_at DATETIME NOT NULL,
        updated_at DATETIME NOT NULL
      );
    SQL
    )
  end
  def self.create_transaction_table(database)
    database.execute(
      <<-SQL
      CREATE TABLE transactions (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        amount INTEGER NOT NULL,
        data BLOB NOT NULL,
        created_at DATETIME NOT NULL,
        updated_at DATETIME NOT NULL
      );
    SQL
    )
  end

  def self.seed(parser, database)
    insert = <<-SQL
      INSERT INTO balance (code, balance_left)
      values (NULL,?,?,DATETIME('now'), DATETIME('now'))
    SQL

    parser.each do |balance|
      database.execute(
        insert,
        balance.code,
        balance.balance_left
      )
    end
  end
end

